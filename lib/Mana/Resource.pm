package Mana::Resource;

use Modern::Perl;

use Dancer2;
use Dancer2::Plugin::Database;
use Dancer2::Plugin::REST;

use Mana::Resource::Subscription;
use Mana::Resource::Review;
use Mana::Resource::Report;
use Mana::Resource::User;
use Mana::Resource::Resource_comment;
use Mana::Resource::Reading_pair;
use Mana::Resource::Librarian;
use constant VALID_RESOURCE => ( qw( review reading_pair subscription report resource_comment ) );
#put in VALID_RESOURCE all resources which are browsable
use constant BULK_IMPORTABLE_RESOURCE => ( qw( reading_pair ) );
#put in BULK_IMPORTABLE_RESOURCE all resource which can be imported through the route bulk import

use DateTime;

require Exporter;
my @ISA = qw(Exporter);
my @EXPORT_OK = qw(isValidResource);



=head1 NAME

Resource.pm - Main module for Mana resources Management

=head1 DESCRIPTION

This module Manages resources: importing, adding, deleting etc. All functions are called by the Interface Mana.pm

=head1 FUNCTIONS


=head2 getEntity

getEntity - select a resource from his ID

=head3 parameters

$params: 2 attributes:
- resource: the name of the desired resource
- id: the id of the desired resource

$want_token: a flag if you want to get the security token (confidential).

=head3 returned value

a hash containing following attributes:
- data: the desired resource
- statuscode: the statuscode which has to be returned by the API

=head3 required functions in package Mana::Resource::foo

none

=cut

sub getEntity {
    my ($params, $want_token) = @_;
    my $rows;
    eval {$rows = database->quick_select($params->{resource}, { id => $params->{id} }); };

    if ($params->{usecomments}){
        $rows->{comments} = getCommentsFor($params->{resource}, $params->{id});
    }

    if ($@) {
        return { msg => "Error : $@", statuscode => 500 };
    }
    delete $rows->{securitytoken} unless $want_token;
    my $return = { data => $rows, statuscode => 200 };
    return $return;
}

=head2 postEntity

postEntity - store a given resource in database

=head3 parameters

$resource: the name of the desired resource
$content: the resource you want to post

content must obey to rules defined in the specific function Mana::Resource::foo->isValidInput;


=head3 returned value

a hash containing following attributes:
- id: the id of the created or existing resource
- msg: the msg which has to be returned by the API
- statuscode: the statuscode which has to be returned by the API

=head3 required functions in package Mana::Resource::foo

isValidInput: check the data which is about to be stored is consistent and complete
resourceAlreadyExists: check the store won't duplicate an existing data
dataInit: initialize non given fields (creation date, nb of users...): default value can be different from default value from database

=cut




sub postEntity {
    my ($resource, $content) = @_;

    my $securitytoken = delete $content->{securitytoken};
    my $package = 'Mana::Resource::' . ucfirst($resource);
    my $isValidInput = $package->isValidInput($content);
    unless ($isValidInput){
        return {msg => "Invalid input on $resource", statuscode => 406 };
    }
    my $id = $package->resourceAlreadyExists($content);
    unless ( $content->{exportemail} ){
        $content->{exportemail}= database->quick_lookup("librarian", { id => $securitytoken }, 'email' ) if $securitytoken;
    }

    unless ($id) {
        database->quick_update("librarian", { id => $securitytoken}, { lastshare => DateTime->now->ymd, nbaccess => \"nbaccess + 1" });
        $content->{securitytoken} = $securitytoken;
        $content = $package->dataInit( $content );
        eval{ database->quick_insert($resource, $content); };
        if ($@) {
            return status_internal_server_error("Error : $@");
        }
        $id = database->last_insert_id(undef, undef, qw($resource id));
        return { msg => "Shared successfuly! Thanks for your help", id => $id, statuscode => 201 };
    }
    return { msg => "The $resource already exists on Mana, thanks for your contribution!", id => $id, statuscode => 208 };
}

=head2 search

search - search a group of resource corresponding to specifications

=head3 parameters

$params: the criterias used to search the resource

the fields in $params depend on the resource you are seaching for. Check the function specificSearch in the dedicated package for more informations.

=head3 returned value

a hash containing following attributes:
- data: a hashref with all matching datas
- msg: the msg which has to be returned by the API
- statuscode: the statuscode which has to be returned by the API

=head3 required functions in package Mana::Resource::foo

specificSearch: defined by default as searching in the specified field matching to a list of searchable field. See this->specific_search for more informations.

=cut



sub search {
    my ($params) = @_;
    my $package = 'Mana::Resource::' . ucfirst($params->{resource});
    my $data;
        $data = $package->specificSearch($params);

    if ($params->{ usecomments }) {
        foreach my $row (@{$data}){
	    $row->{comments} = getCommentsFor($params->{resource}, $row->{id});
        }
    }

    if ($@) {
        return { msg =>"Error : $@", statuscode => 500 };
    }
    return { data => $data };
}

=head2 incrementField

incrementField - Increment the specified field of the specified resource

=head3 parameters

$params: a hash with following attributes
id: the id of incremented resource (required)
field: the field to increment (required)
resource: the name of the resource (required)
step: the step of the incrementation (default 1)

=head3 returned value

a hash containing following attributes:
- id: the id of the incremented resource (if successful)
- msg: the msg which has to be returned by the API
- statuscode: the statuscode which has to be returned by the API

=head3 required functions in package Mana::Resource::foo

specificSearch: defined by default as searching in the specified field matching to a list of searchable field. See this->specific_search for more informations.

=cut


sub incrementField {
    my ($params) = @_;
    my $resource = lcfirst( $params->{resource} );
    my $field = $params->{field};
    my $id = $params->{id};
    my $step = $params->{step} || 1;
    my $fields_to_update;
    $fields_to_update->{$field}= \"$field + $step";
    if ($field  eq 'nbofusers'){
        $fields_to_update->{lastimport}=DateTime->now->ymd;
    }
    my $package = 'Mana::Resource::'. ucfirst($resource);
    eval {
        database->quick_update(
            $resource,
            { id => $id },
            $fields_to_update
        );
    };

    if ($@) {
        return { msg =>"Error : $@", statuscode => 500};
    }
    return {id => $id, statuscode => 200};
}

=head2 getCommentsFor

getCommentsFor - Retrieves all comments for a specific ressource and id.

=cut

sub getCommentsFor {
    my ($resource, $id) = @_;

    my $query = "SELECT * FROM resource_comment r WHERE r.resource_type = ? AND r.resource_id = ?";
    my $sth = database->prepare($query);
    $sth->execute( $resource, $id );

    return $sth->fetchall_arrayref({});
}

=head2 deleteComments

deleteComments - delete all comments related to a given resource

=cut

sub deleteComments {
    my $resource = shift;
    my $id = shift;
    eval {
        my $query = "DELETE FROM resource_comment WHERE resource_id=? AND resource_type=?";
        my $sth = database->prepare($query);
        $sth->execute($id, $resource);
    }
}

=head2 deleteResource

deleteResource - delete a resource and the comments related to

=cut

sub deleteResource{
    my $resource = shift;
    my $id = shift;
    my $package = "Mana::Resource::".ucfirst($resource);
    $package->destructor($id);
    eval {
        database->quick_delete( $resource, { id => $id });
        deleteComments( $resource, $id );
    };
    return status_ok(to_json( { id => $id, statuscode => 200}) )
        unless $@;
    return status_500( to_json( { statuscode => 500, msg => "Error: $@"} ) );
}

=head2 isValidResource

isValidResource - verify the resource exists and can be edited

=cut

sub isValidResource {
    my ($resource) = @_;
    return ( grep { $_ eq $resource } VALID_RESOURCE );
}

sub isBulkImportableResource {
    my ($self, $resource) = @_;
    return ( grep { $_ eq $resource } BULK_IMPORTABLE_RESOURCE );
}

sub getValidResource {
    return VALID_RESOURCE;
}


#############################################################################################################

                ## DEFAULT BEHAVIOUR FOR ALL RESOURCE ##

#############################################################################################################



sub isValidInput {
    my ($self, $content) = @_;
    my @keys = keys %$content;
    foreach my $required_entry ( $self->getValidEntries() ){
        unless ( grep {$_ eq $required_entry} @keys ){
            return 0;
        }
    }
    return 1;
}


sub specificSearch {
    my ($self, $params) = @_;
    my $resourceName = $self->getResourceName();
    my $query = "SELECT * FROM $resourceName WHERE 1";

    my @keys;
    my @values;
    my $filtered_params = $self->filterParamsForSearch($params);
    while ( my ($key, $value) = each(%$filtered_params) ) {
        if (defined $value) {
            my @words = split / /, $value;
            foreach my $word (@words) {
                push @keys, $key." LIKE ?";
                push @values, "%$word%";
            }
        }
    }

    if ( scalar @keys ne 0 ) {
        $query .= " AND " . join(" AND ", @keys);
    }

    my $sth = database->prepare($query);
    $sth->execute(@values);
    my $rows = $sth->fetchall_arrayref({});
    return $rows;
}

sub filterParamsForSearch {
    my ($self, $params) = @_;

    my $filtered_params = {};
    while ( my ($key, $value) = each(%$params) ) {
        if ( grep { $_ eq $key  } $self->getValidParams() ) {
            $filtered_params->{ $key } = $value;
        }
    }

    return $filtered_params;
}

sub destructor {
    return 1;
}

1;
