package Mana::Interface;

use Modern::Perl;

use Dancer2;
use Template;
use Dancer2::Plugin::Passphrase;
use Dancer2::Plugin::Database;
use Dancer2::Plugin::REST;
use Email::Sender::Simple qw(sendmail);
use Email::MIME;

use Template;

use HTTP::Request::Common;
use LWP::UserAgent;

use Bytes::Random::Secure qw(
    random_bytes random_bytes_base64 random_bytes_hex random_bytes_qp random_string_from
);


our $VERSION = '0.1';

sub checkLogin {
    my ($params) = @_;
    my $login = $params->{ login };
    my $password = $params->{ password };

    my $user = database->quick_select('user', { login => $login });

    if ($user && passphrase($password)->matches($user->{password}) ) {
        return 1;
    }

    if ( config->{demo} && $login eq 'demo' && $password eq 'demo' ) {
        return 1;
    }

    return 0;
}

sub sendConfirmation {
    my $params = shift;
    my $allowed_character = '1234567890azertyuiopmlkjhgfdsqwxcvbnAZERTYUIOPQSDFGHJKLMWXCVBN';
    my $emailAddress = $params->{email};
    my $name = $params->{name};
    my $domain = config->{mana_domain};
    my $today = DateTime->now->ymd;
    my $token = random_string_from( $allowed_character, 32 );
    my $rand           = random_bytes_qp( 32 );
    my $reclamationAddress = "$domain/test2?id=$token";
    my $validationAddress = "$domain/registration?id=$token";
    my $manaEmail = config->{mana_email};
    eval{ database->quick_insert('librarian', {
            id => $token,
            name => $name,
            creationdate => $today,
            email => $emailAddress
        })
    };

    if ($@ ){
        return status_internal_server_error("Error: $@");
    }

    # first, create your message
    my $message = Email::MIME->create(
        header_str => [
            From    => $manaEmail,
            To      => "$emailAddress",
            Subject => 'Mana KB registration',
        ],
        attributes => {
            encoding => 'quoted-printable',
            charset  => 'ISO-8859-1',
        },
        body_str => "    Hello $name,\n    Someone asked on $today for an authentication on the Mana KB webservice. To activate your account definitively, please click on the following link and follow the instruction $validationAddress\n\n"); 

=for comment
If you are not responsible for this demand, or if you are not $name, please use the link here: $reclamationAddress\n\nSee you soon on the ManaKB service!\nThe manaKB Team\n\nThis message has been generated automatically, please to not answer to this mail"
        );
=cut

        eval { my $result = sendmail($message); };
        if ($@) {
            warn "Unable to send email";
        }

        status_created( { token => $token });
}

sub confirmAccount {
    my $params = shift;
    my $ua = LWP::UserAgent->new;
    my $privatekey = config->{captcha_key};
    my $response;
    if ( $params->{'g-recaptcha-response'} ){
        $response = $params->{'g-recaptcha-response'};
    }
    else{
        return 'failedcaptcha';
    }
    my $result = $ua->request(POST 'https://www.google.com/recaptcha/api/siteverify', [response => $response, secret => $privatekey]); 
    my $captcharesult = from_json( $result->content )->{ success };
    unless ($captcharesult){
        return 'failedcaptcha';
    }
    my $activationdate;
    eval { $activationdate = database->quick_select("librarian", { id => $params->{id}})->{activationdate}; };
    if ( $@ ){
        return "tokennotfound";
    }

    if ( $activationdate ){
        return "alreadyactivated";
    }
    else {
        eval { database->quick_update("librarian", { id => $params->{id} }, {activationdate => DateTime->now->ymd}); };
        if ( $@ ){
            return "tokennotfound";
        }
    }
    return "success";
}


1;
