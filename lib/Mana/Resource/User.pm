package Mana::Resource::User;

use Dancer2;
use Dancer2::Plugin::Database;
use Dancer2::Plugin::Passphrase;
use Mana::Resource;
use vars qw( @ISA ); @ISA = qw( Mana::Resource );

our @searchable_field = ('id', 'email', 'login');

our @accepted_values = ('id', 'email', 'login', 'password');

=head1 NAME

Mana::Resource::User - Users gestion methods

=head1 METHODS

=cut

=head3 getValidParams

    my @params = mana::Resource::User->getValidParams;

=cut

sub getValidParams { return @searchable_field}

=head3 specificSearch

    my $users = mana::Resource::User->specificSearch( { email => $email});

=cut

sub specificSearch {
    my ($self, $params) = @_;

    my $filtered_params = $self->filterParamsForSearch($params);

    my @users = database->quick_select('user', $filtered_params);

    return \@users;
}

=head3 add

    mana::Resource::User->add( $values );

=cut

sub add {
    my ($self, $values) = @_;

    if ( $values->{password} ) {
	my $password = $values->{password};
	my $phrase = passphrase( $password )->generate;
	$values->{password} = $phrase->rfc2307();
    }

    my $filtered_values = {};
    while ( my ($key, $value) = each(%$values) ) {
        if ( grep { $_ eq $key  } @accepted_values ) {
            $filtered_values->{ $key } = $value;
        }
    }

    database->quick_insert('user', $filtered_values);
}

1;
