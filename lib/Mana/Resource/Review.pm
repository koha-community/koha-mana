package Mana::Resource::Review;

use Modern::Perl;
use Mana::Resource;
use Dancer2;
use Dancer2::Plugin::Database;
use vars qw( @ISA ); @ISA = qw( Mana::Resource );


#please with the resource name
sub getResourceName{ return lc("Review"); }

#please complete the params threw which you can search in the database.
our @valid_params = qw( review documentid idtype language );

sub getValidParams{
   return @valid_params;
}

our @required_entries  = qw( review reviewid documentid idtype language );

sub getValidEntries {
    return @required_entries;
}



=head1 NAME

Mana::Resource::Review - Review gestion methods

=head1 SYNOPSIS

(you can override all this methods);
my $content;
mana::Resource::Review->isValidInput($content);
mana::Resource::Review->dataInit($content);
mana::Resource::Review->specific_search($params);
mana::Resource::Review->resourceAlreadyExists($params);
mana::Resource::Review->filterParamsForSearch($params);
mana::Resource::Review->destructor($id);

=cut

sub resourceAlreadyExists {
    my ($self, $content) = @_;
    my $content_to_check;
    $content_to_check->{reviewid}=$content->{reviewid};
    $content_to_check->{documentid}=$content->{documentid};
    $content_to_check->{review} = {
        like => substr($content->{review},0,10)."%"
    };
    my $id = database->quick_lookup( 'review', $content_to_check, 'id');
    return $id;
}

sub dataInit {
    my ($self, $content) = @_;
    $content->{creationdate} = DateTime->now->ymd;
    return $content;
}

1;
