package Mana::Resource::Subscription;

use Modern::Perl;
use Dancer2;
use Dancer2::Plugin::Database;
use Mana::Resource;
use vars qw( @ISA ); @ISA = qw( Mana::Resource );

our @valid_params = (
    'issn', 'ean', 'title', 'language'
);

our @required_entries  = (
    'title', 'publishercode', 'language'
);

our $resource_name = 'subscription';

=head1 NAME

Mana::Resource::Subscription - Subscription gestion methods

=head1 SYNOPSIS

mana::Resource::Subscription->dataInit($content);
mana::Resource::Subscription->search($params);
mana::Resource::Subscription->resourceAlreadyExists($params);
mana::Resource::Subscription->filterParamsForSearch($params);
mana::Resource::Subscription->destructor($id);

=head1 GENERAL COMMENTS ON RESOURCES

All resources must have this interface in order to be manipulated by the api. 

=head1 METHODS

=cut

=head3 getValidParams

    my @params = mana::Resource::Subscription->getValidParams;

=cut

sub getValidParams { return @valid_params;}

=head3 getValidEntries

    my @required_entries = mana::Resource::getValidEntries;;

=cut

sub getValidEntries { return @required_entries;}

=head3 getResourceName

    my $ressource = mana::Resource::getResourceName;

=cut

sub getResourceName { return $resource_name };

=head3 dataInit 

    $content = $package->dataInit( $content );

=cut

sub dataInit {
    my $self = shift;
    my $content = shift;
    $content->{lastimport} = DateTime->now->ymd;
    $content->{creationdate} = DateTime->now->ymd;
    $content->{nbofusers} = 1;
    return $content;
}

=head3 resourceAlreadyExists 

    my $id = $package->resourceAlreadyExists($content);

=cut

sub resourceAlreadyExists {
    my ($self, $content) = @_;
    my $bulk_import = delete $content->{bulk_import};
    my $content_to_check = { %$content };
    delete $content_to_check->{exportemail};
    delete $content_to_check->{sndescription};
    delete $content_to_check->{sfdescription};
    delete $content_to_check->{numberingmethod};
    delete $content_to_check->{creationdate};
    delete $content_to_check->{lastimport};
    delete $content_to_check->{nboferror};
    if ($bulk_import) {
        delete $content_to_check->{label};
    }
    if ( my $existing = database->quick_select('subscription', $content_to_check) ) {
        return $existing->{id};
    }
}

=head3 specificSearch 

    my $results =  mana::Resource::Subscription->specificSearch($params);

=cut

sub specificSearch {
    my ($self, $params) = @_;
    my $resourceName = $self->getResourceName();
    my $query = "SELECT * FROM subscription WHERE 1";

    my @keys;
    my @values;
    my $filtered_params = $self->filterParamsForSearch($params);
    while ( my ($key, $value) = each(%$filtered_params) ) {
        if (defined $value) {
	    push @keys, $key." LIKE ?";
	    push @values, "%$value%";
        }
    }

    if ( scalar @keys ne 0 ) {
        $query .= " AND (" . join(" OR ", @keys) . ")";
    }

    my $sth = database->prepare($query);
    $sth->execute(@values);
    my $rows = $sth->fetchall_arrayref({});
    return $rows;
}

1;
