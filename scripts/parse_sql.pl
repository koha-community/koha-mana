#!/usr/bin/perl

# This file is part of MANA.
#
# Koha is free software; you can redistribute it and/or modify it
# under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# Koha is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Koha; if not, see <http://www.gnu.org/licenses>.

=head1 NAME

parse_sql.pl - Parses the wiki and extracts informations

=head1 SYNOPSIS

parse_sql.pl filename

=head1 DESCRIPTION

The script parses the page containing all reports of the wiki, given threw filename and store them in a importable csv. This script isn't so reusable.

=cut

use Modern::Perl;
use Getopt::Long;
use Pod::Usage;
use Text::CSV;

use Dancer2;
use Dancer2::Plugin::Database;
use DateTime;
use Time::HiRes;


use Mana::Resource;

my ($help, $language, $verbose);
GetOptions(
    'help|?'        => \$help,
    'language|l:s'  => \$language,
    'verbose|v'     => \$verbose,
) or pod2usage(2);

$language ||= 'en';

my $cter = 0;
my $progress = 0;
my $resource = "subscription";
my $begin = DateTime->now;
my $beginns = Time::HiRes::time();
my $report;
my $key;
my @wordlist;
my $bind = { Module => "report_group", Purpose => "notes", Title => "report_name", savedsql => "savedsql", language => "language" };
my $sql;
my $csv = Text::CSV->new({ eol => "$/"});
my @attributelist = values %$bind;

open (my $fh, '<:encoding(UTF-8)', "scripts/texttoimport.txt") or die "couldn't open file";
open (my $fh2, '>:encoding(UTF-8)', "scripts/csvtoimport.csv") or die "couldn't open file";
$csv->column_names( \@attributelist );
$csv->print( $fh2, \@attributelist );
while(my $line = <$fh>) {
    chomp ($line);
    $cter++;
    eval {$line =~ s/'''//g};
    eval {$line =~ s/====/Title:/};
    eval {$line =~ s/====//};
    eval {$line =~ s/===/Title:/};
    eval {$line =~ s/===//};
    eval {$line =~ s/==/Title:/};
    eval {$line =~ s/==//};
    if ( substr( $line, 0, 1) eq '*' ) { $line =~ s/\* // };
    @wordlist = split (' ', $line);
my @keys = keys %$bind;
    if ( $wordlist[0] ){
        my $temp = $wordlist[0];
        $temp =~ s/://;
        if ( $temp =~ /^(Module|Purpose|Title)$/ ){
           $key = $bind->{ $temp };
           eval { $line =~ s/$wordlist[0]:// };
           $temp = $wordlist[0].' ';
           $line =~ s/$temp//;
           $line =~ s/"/'/g;
           $report->{ $key } = $line;
        }elsif ( $line =~ /<syn/) {
            $sql = '';
            $line = <$fh>;
            while ( not( $line =~ /<\/syn/ ) ){
               chomp($line);
               $sql = $sql.$line;
               $line = <$fh>;
            }
            $report->{savedsql} = $sql;
            $report->{language} = "EN";
            $csv->print_hr($fh2, $report);
        }
    }
}
close $fh;
close $fh2;

my $duration = DateTime->now - $begin;
my $durationns = (Time::HiRes::time() - $beginns);
$durationns = $durationns - int $durationns;
$durationns = int $durationns * 1000000;
