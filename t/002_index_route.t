use strict;
use warnings;


use Mana;
use Test::More tests => 6;
use Plack::Test;
use Dancer2;
use Dancer2::Plugin::Database;
use Dancer2::Plugin::Passphrase;
use HTTP::Request::Common;



my $app = Mana->to_app;
is( ref $app, 'CODE', 'Got app' );

database->{AutoCommit} = 0;
my $test = Plack::Test->create($app);
database->quick_insert( "user", { login => "login", password => passphrase("psswd")->generate->rfc2307} );

my $res  = $test->request( GET "/login" );
ok( $res->is_success, "[GET /login successful" );

$res  = $test->request(POST "/login?login=login&password=psswd");
ok( $res->is_redirect, "[POST /login] successful" );

$res  = $test->request( GET "/" );
ok( $res->is_redirect, "[GET /] successful" );

$res  = $test->request( GET "/admin/report" );
ok( $res->is_redirect, "[GET /admin/report] successful" );

$res  = $test->request( GET "/admin/subscription" );
ok( $res->is_redirect, "[GET /admin/subscription] successful" );

eval{ database->rollback; };
database->{AutoCommit} = 1;

1;
