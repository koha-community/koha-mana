#! /usr/bin/perl -w

use Modern::Perl;

use Mana;
use Test::More tests => 5;
use Plack::Test;
use HTTP::Request::Common;
use Dancer2;
use Dancer2::Plugin::Database;
use Dancer::Logger;
use Data::Dumper;
use Dancer2::Plugin::REST;
use Dancer2::Serializer::JSON;
use utf8;

my $app = Mana->to_app;

my $test = Plack::Test->create($app);

database->{AutoCommit} = 0;
database->quick_delete("resource_comment", 1);
database->quick_insert("librarian", { id => '1234', email => 'xx.xx@xx.com', firstname => 'Nathalie', lastname => 'Portman', creationdate => '2017-04-19', activationdate => '2017-04-19'});

my $newReport2;
$newReport2->{savedsql} ="Select car from parking" ;
$newReport2->{report_name} = "parking" ;
$newReport2->{notes} = "Count cars from parking";
$newReport2->{type} = "carsCounter";
$newReport2->{securitytoken} = "1234";
$newReport2->{report_group} = "carParking";
$newReport2->{language} = "FR";

my $newJsonReport2 = to_json($newReport2, { utf8 => 1 });
$test->request( POST '/report.json', Content => $newJsonReport2 );

my $id = database->quick_select("report", 1)->{id};

my $newSubscription;
$newSubscription->{title} ="this is a title" ;
$newSubscription->{issn} ="this is an issn" ;
$newSubscription->{ean} ="ean" ;
$newSubscription->{publishercode} = "this is a publishercode" ;
$newSubscription->{securitytoken} = '1234';
my $newSubscriptionShort;
%$newSubscriptionShort = %$newSubscription;

$newSubscription->{numberingmethod} = "this is a numbering method" ;
$newSubscription->{sfdescription} = "this is a description" ;
$newSubscription->{unit} = "day" ;
$newSubscription->{exportemail} = 'XX@xx.com' ;
$newSubscription->{kohaversion} = '0.0' ;
$newSubscription->{language} = 'ouzbek' ;
$newSubscription->{label1} = "this is the label1" ;
$newSubscription->{add1} = 1;
$newSubscription->{every1} = 1;
$newSubscription->{whenmorethan1} = 1 ;
$newSubscription->{setto1} = 1 ;
$newSubscription->{numbering1} = "this is a numbering" ;
$newSubscription->{label2} = "this is the label2" ;
$newSubscription->{add2} = 2;
$newSubscription->{every2} = 2;
$newSubscription->{whenmorethan2} = 2 ;
$newSubscription->{setto2} = 2 ;
$newSubscription->{numbering2} = "this is a numbering2" ;
$newSubscription->{label3} = "this is the label3" ;
$newSubscription->{add3} = 3;
$newSubscription->{every3} = 3;
$newSubscription->{whenmorethan3} = 3 ;
$newSubscription->{setto3} = 3 ;
$newSubscription->{numbering3} = "this is a numbering3" ;

my $newJsonSubscription = to_json($newSubscription, { utf8 => 1 });
$test->request( POST '/subscription.json', Content => $newJsonSubscription );
my $subscriptionid = database->quick_select("subscription", 1)->{id};




my $newResource_Comment;
$newResource_Comment->{message} ="This is a message" ;
$newResource_Comment->{resource_id} = $id;
$newResource_Comment->{resource_type} = "report";
$newResource_Comment->{nb} = 1;
$newResource_Comment->{securitytoken} = '1234';
$newResource_Comment->{exportemail} = 'xx.xx@xx.com';

my $newResource_Comment2;
$newResource_Comment2->{message} ="This is a second message" ;
$newResource_Comment2->{resource_id} = 23434543456565534563456345634563454345345;
$newResource_Comment2->{resource_type} = "report";
$newResource_Comment2->{nb} = 1;
$newResource_Comment2->{securitytoken} = '1234';
$newResource_Comment2->{exportemail} = 'xx.xx@xx.com';

my $newResource_Comment3;
$newResource_Comment3->{message} ="This is a third message" ;
$newResource_Comment3->{resource_id} = 10 ;
$newResource_Comment3->{resource_type} = "wrong_resource_name";
$newResource_Comment3->{nb} = 1;
$newResource_Comment3->{securitytoken} = '1234';
$newResource_Comment3->{exportemail} = 'xx.xx@xx.com';

my $newResource_Comment4;
$newResource_Comment4->{message} ="This is a 4th message" ;
$newResource_Comment4->{resource_id} = $subscriptionid ;
$newResource_Comment4->{resource_type} = "subscription";
$newResource_Comment4->{nb} = 1;
$newResource_Comment4->{securitytoken} = '1234';
$newResource_Comment4->{exportemail} = 'xx.xx@xx.com';


my @attributelist = keys %$newResource_Comment;

subtest 'critical cases for Share function' => sub {
    plan tests => 3;
    
    my $temp = delete $newResource_Comment->{message};
    my $newJsonResource_Comment = to_json($newResource_Comment, { utf8 => 1 });
    my $nbbeforeinsert = database->quick_count("report", 1);
    my $resJson = $test->request( POST '/report.json', Content => $newJsonResource_Comment );
    my $res = from_json( $resJson->content );

    subtest 'attribute message missing' => sub {
        plan tests => 3;
        is( $resJson->code, 406, "right error code");
        ok( $res->{msg} =~ /Invalid input on/, "propper error message");

        #nothing were added
        is( database->quick_count( "report", 1 ), $nbbeforeinsert, "database is the same after error");
    };

    $newResource_Comment->{message} = $temp;

    $temp = delete $newResource_Comment->{resource_id};
    $newJsonResource_Comment = to_json($newResource_Comment, { utf8 => 1 });
    $nbbeforeinsert = database->quick_count("report", 1);
    $resJson = $test->request( POST '/report.json', Content => $newJsonResource_Comment );

    subtest 'attribute resource_id missing' => sub {
        is( $resJson->code, 406, "right error code");
        $res = from_json( $resJson->content );
        ok( $res->{msg} =~ /Invalid input on/, "propper error message");

        #nothing were added
        is( database->quick_count( "report", 1 ), $nbbeforeinsert, "database is the same after error");
    };
    $newResource_Comment->{resource_id} = $temp;

    $temp = delete $newResource_Comment->{resource_type};
    $newJsonResource_Comment = to_json($newResource_Comment, { utf8 => 1 });
    $nbbeforeinsert = database->quick_count("report", 1);
    $resJson = $test->request( POST '/report.json', Content => $newJsonResource_Comment );

    subtest 'attribute resource_type missing' => sub {
        is( $resJson->code, 406, "right error code");
        $res = from_json( $resJson->content );
        ok( $res->{msg} =~ /Invalid input on/, "propper error message");

        #nothing were added
        is( database->quick_count( "report", 1 ), $nbbeforeinsert, "database is the same after error");
    };
    $newResource_Comment->{resource_type} = $temp;
};




subtest 'Share function' => sub {
    plan tests => 3;

    my $nbbeforeinsert = database->quick_count("resource_comment", 1);
    my $newJsonResource_Comment = to_json($newResource_Comment, { utf8 => 1 });
    my $newJsonResource_Comment2 = to_json($newResource_Comment2, { utf8 => 1 });
    my $newJsonResource_Comment3 = to_json($newResource_Comment3, { utf8 => 1 });
    my $newJsonResource_Comment4 = to_json($newResource_Comment4, { utf8 => 1 });
    my $resJson = $test->request( POST '/resource_comment.json', Content => $newJsonResource_Comment );
    my $resJson2 = $test->request( POST '/resource_comment.json', Content => $newJsonResource_Comment2 );
    my $resJson3 = $test->request( POST '/resource_comment.json', Content => $newJsonResource_Comment3 );
    my $resJson4 = $test->request( POST '/resource_comment.json', Content => $newJsonResource_Comment4 );
    my $resdb = database->quick_select("resource_comment", $newResource_Comment);
    my $nbafterinsert = database->quick_count("resource_comment", 1);

    is( $nbafterinsert, $nbbeforeinsert + 2, "added something in database");
    my $res = from_json( $resJson->content );

    foreach my $key ( keys %$resdb){
        delete $resdb->{$key} unless grep {$key eq $_} @attributelist;
    }
    is_deeply( $resdb, $newResource_Comment, "Sucessfuly added");

        #test about the error returned by function
    is( $res->{'error'}, undef , "no error returned");
   
};

$newResource_Comment->{creationdate} = DateTime->now->ymd;
$newResource_Comment->{id} = database->quick_select("resource_comment", $newResource_Comment)->{id};

subtest "Get by id Route" => sub {
    plan tests => 2;
    my $resJson = $test->request( GET "/resource_comment/$newResource_Comment->{id}.json" );
    my $res = from_json( $resJson->content );
    my $resdata = $res->{data};
    $resdata->{securitytoken} = '1234'; #get with id doesn't return the securuty token

        #test about expected values of the fields
    is_deeply($newResource_Comment, $resdata, "successfuly got with route");

        #test about the error returned by function
    is( $res->{'error'}, undef , "no error returned");
};

subtest "Search engine" => sub {

    plan tests => 4;
    my $resJson = $test->request( GET "/resource_comment.json?resource_type=report&resourceid=$id");
    my $resJson2 = $test->request( GET "/resource_comment.json?resource_type=wrong_resource_type&resourceid=10");
    my $res = from_json( $resJson->content );

    my $res2 = from_json( $resJson2->content );
    my $resdata = $res->{data};
    my $resdata2 = $res2->{data};

my $indb = Data::Dumper::Dumper(database->quick_select("resource_comment", $newResource_Comment));
    is_deeply($resdata->[0], $newResource_Comment, "research successful");
    is(scalar keys @$resdata, 1, "only the expected search");
        #test about the error returned by function
    is($resdata2->[0], undef, "search on wrong resource returns nothing");

    is( $res->{'error'}, undef , "no error returned");
 
};

subtest "increment field" => sub {
    plan tests => 2;
    
    my $id = database->quick_select("resource_comment", $newResource_Comment)->{id};
    my $resJson = $test->request( POST "/resource_comment/$id.json/increment/nb?securitytoken=$newResource_Comment->{securitytoken}" );


    my $incrementedResource_CommentInDB = database->quick_select("resource_comment", { id => $id } );
    is( $incrementedResource_CommentInDB->{nb}, 2, "correctly incremented" ); 

    $resJson = $test->request( POST "/resource_comment/$id.json/increment/nb?step=2&securitytoken=$newResource_Comment->{securitytoken}");
    $incrementedResource_CommentInDB = database->quick_select("resource_comment", { id => $id } );
    is( $incrementedResource_CommentInDB->{nb}, 4, "correctly incremented" );

};



eval{ database->rollback; };
database->{AutoCommit} = 1;
1;
