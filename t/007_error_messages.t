#! /usr/bin/perl -w

use Modern::Perl;

use Mana;
use Test::More tests => 2;
use Plack::Test;
use HTTP::Request::Common;
use Dancer2;
use Dancer2::Plugin::Database;
use Dancer::Logger;
use Data::Dumper;
use Dancer2::Plugin::REST;
use Dancer2::Serializer::JSON;
use utf8;

my $app = Mana->to_app;

my $test = Plack::Test->create($app);

my $newReport;
$newReport->{savedsql} ="Select pommes from arbre" ;
$newReport->{report_name} = "verger" ;
$newReport->{report_group} = "CiderBrew" ;
$newReport->{notes} = "Ceci compte les pommes du verger";
$newReport->{type} = "pommesCounter";
$newReport->{securitytoken} = 1234;
$newReport->{language} = "FR";


database->{AutoCommit} = 0;
database->quick_delete("subscription", 1);
database->quick_delete("librarian", 1);

subtest "error for no token" => sub {
    plan tests =>2;
    
    my $newJsonReport = to_json( $newReport );
    my $resJson = $test->request( POST '/report.json', Content => $newJsonReport );

    is( $resJson->code, 401, "right status code");
    my $res = from_json( $resJson->content );
    ok( $res->{msg} =~ /Invalid security token/, "right message" );
};

subtest "error for wrong resource" => sub {
    plan tests => 2;
    
    database->quick_insert("librarian", { id => 1234, email => 'xx.xx@xx.com', firstname => 'Nathalie', lastname => 'Portman', creationdate => '2017-04-19', activationdate => '2017-04-19'});
    my $newJsonReport = to_json( $newReport );
    my $resJson = $test->request( POST '/reports.json', Content => $newJsonReport );

    is( $resJson->code, 400, "right status code");
    my $res = from_json( $resJson->content );
    ok( $res->{msg} =~ /The resource/, "right message" );
};



database->rollback;
database->{AutoCommit} = 1;
