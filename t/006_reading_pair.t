#! /usr/bin/perl -w

use Modern::Perl;

use Mana;
use Test::More tests => 7;
use Plack::Test;
use HTTP::Request::Common;
use Dancer2;
use Dancer2::Plugin::Database;
use Dancer::Logger;
use Data::Dumper;
use Dancer2::Plugin::REST;
use Dancer2::Serializer::JSON;
use utf8;

my $app = Mana->to_app;

my $test = Plack::Test->create($app);

database->{AutoCommit} = 0;
my $sth = database->prepare( "truncate reading_pair" );
$sth->execute();

database->quick_insert("librarian", { id => 1234, email => 'xx.xx@xx.com', firstname => 'Nathalie', lastname => 'Portman', creationdate => '2017-04-19', activationdate => '2017-04-19'});

my $newReading_pair;
$newReading_pair->{documentid1} ="id01" ;
$newReading_pair->{documentid2} = "id02" ;
$newReading_pair->{idtype1} = "idtype01" ;
$newReading_pair->{idtype2} = "idtype02";
$newReading_pair->{securitytoken} = "1234";
my @attributelist = keys %$newReading_pair;


subtest 'critical cases for Share function' => sub {
    plan tests => 4;
    
    my $temp = delete $newReading_pair->{documentid1};
    my $newjsonReading_pair = to_json($newReading_pair, { utf8 => 1 });
    my $nbbeforeinsert = database->quick_count("reading_pair", 1);
    my $resjson = $test->request( POST '/reading_pair.json', content => $newjsonReading_pair );
    my $res = from_json( $resjson->content );
    subtest 'attribute documentid1 missing' => sub {
        is( $resjson->code, 406, "right error code");
        $res = from_json( $resjson->content );
        ok( $res->{msg} =~ /Invalid input on/, "propper error message");

        #nothing were added
        is( database->quick_count( "reading_pair", 1 ), $nbbeforeinsert, "database is the same after error");
    };
    $newReading_pair->{documentid1} = $temp;
    $temp = delete $newReading_pair->{documentid2};
    $newjsonReading_pair = to_json($newReading_pair, { utf8 => 1 });
    $nbbeforeinsert = database->quick_count("reading_pair", 1);
    $resjson = $test->request( POST '/reading_pair.json', content => $newjsonReading_pair );
    $res = from_json( $resjson->content );

    subtest 'attribute documentid2 missing' => sub {
        is( $resjson->code, 406, "right error code");
        $res = from_json( $resjson->content );
        ok( $res->{msg} =~ /Invalid input on/, "propper error message");

        #nothing were added
        is( database->quick_count( "reading_pair", 1 ), $nbbeforeinsert, "database is the same after error");
    };
    $newReading_pair->{documentid2} = $temp;

    $temp = delete $newReading_pair->{idtype1};
    $newjsonReading_pair = to_json($newReading_pair, { utf8 => 1 });
    $nbbeforeinsert = database->quick_count("reading_pair", 1);
    $resjson = $test->request( POST '/reading_pair.json', content => $newjsonReading_pair );
    $res = from_json( $resjson->content );

    subtest 'attribute idtype missing' => sub {
        is( $resjson->code, 406, "right error code");
        $res = from_json( $resjson->content );
        ok( $res->{msg} =~ /Invalid input on/, "propper error message");

        #nothing were added
        is( database->quick_count( "reading_pair", 1 ), $nbbeforeinsert, "database is the same after error");
    };
    $newReading_pair->{idtype1} = $temp;

    $temp = delete $newReading_pair->{idtype2};
    $newjsonReading_pair = to_json($newReading_pair, { utf8 => 1 });
    $nbbeforeinsert = database->quick_count("reading_pair", 1);
    $resjson = $test->request( POST '/reading_pair.json', content => $newjsonReading_pair );
    $res = from_json( $resjson->content );

    subtest 'attribute idtype2 missing' => sub {
        is( $resjson->code, 406, "right error code");
        $res = from_json( $resjson->content );
        ok( $res->{msg} =~ /Invalid input on/, "propper error message");

        #nothing were added
        is( database->quick_count( "reading_pair", 1 ), $nbbeforeinsert, "database is the same after error");
    };
    $newReading_pair->{idtype2} = $temp;
};



subtest 'Share function' => sub {
    plan tests => 2;


    my $newJsonReading_pair = to_json($newReading_pair, { utf8 => 1 });
    my $nbbeforeinsert = database->quick_count("reading_pair", 1);
    my $resJson = $test->request( POST '/reading_pair.json', Content => $newJsonReading_pair );
    my $resdb = database->quick_select("reading_pair", $newReading_pair);
    my $nbafterinsert = database->quick_count("reading_pair", 1);
    is( $nbafterinsert, $nbbeforeinsert + 1, "added something in database");


    foreach my $key ( keys %{$resdb} ){
        delete $resdb->{$key} unless grep {$key eq $_} @attributelist;
    }

        #test about the error returned by function
    my $res = from_json( $resJson->content );
    is( $res->{'error'}, undef , "no error returned");
   
};


my $newReading_pair1;
$newReading_pair1->{documentid1} ="id11" ;
$newReading_pair1->{documentid2} = "id12" ;
$newReading_pair1->{idtype1} = "idtype11" ;
$newReading_pair1->{idtype2} = "idtype12";
$newReading_pair1->{securitytoken} = "1234";
$newReading_pair1->{nb} = 10;

my $newReading_pair2;
$newReading_pair2->{documentid1} ="id21" ;
$newReading_pair2->{documentid2} = "id22" ;
$newReading_pair2->{idtype1} = "idtype21" ;
$newReading_pair2->{idtype2} = "idtype22";
$newReading_pair2->{securitytoken} = "1234";
$newReading_pair2->{nb} = 2;


my $newReading_pair3;
$newReading_pair3->{documentid1} ="id31" ;
$newReading_pair3->{documentid2} = "id32" ;
$newReading_pair3->{idtype1} = "idtype31" ;
$newReading_pair3->{idtype2} = "idtype32";
$newReading_pair3->{securitytoken} = "1234";
$newReading_pair3->{nb} = 1;

my $newReading_pair4;
$newReading_pair4->{documentid1} = "2744106178" ;
$newReading_pair4->{documentid2} = "2744106186" ;
$newReading_pair4->{idtype1} = "ean" ;
$newReading_pair4->{idtype2} = "ean";
$newReading_pair4->{securitytoken} = "1234";
$newReading_pair4->{nb} = 1;


my $newReading_pair5;
$newReading_pair5->{documentid1} ="id22" ;
$newReading_pair5->{documentid2} = "id11" ;
$newReading_pair5->{idtype1} = "idtype22" ;
$newReading_pair5->{idtype2} = "idtype11";
$newReading_pair5->{securitytoken} = "1234";
$newReading_pair5->{nb} = 3;



my $newReading_pair6;
$newReading_pair6->{documentid1} ="id11" ;
$newReading_pair6->{documentid2} = "id31" ;
$newReading_pair6->{idtype1} = "idtype11" ;
$newReading_pair6->{idtype2} = "idtype31";
$newReading_pair6->{securitytoken} = "1234";
$newReading_pair6->{nb} = 15;



#add new reading pairs in the base
    my $newJsonReading_pair1 = to_json($newReading_pair1, { utf8 => 1 });
    $test->request( POST '/reading_pair.json', Content => $newJsonReading_pair1 );

    my $newJsonReading_pair2 = to_json($newReading_pair2, { utf8 => 1 });
    $test->request( POST '/reading_pair.json', Content => $newJsonReading_pair2 );
    my $newJsonReading_pair3 = to_json($newReading_pair3, { utf8 => 1 });
    my $result = $test->request( POST '/reading_pair.json', Content => $newJsonReading_pair3 );
    my $newJsonReading_pair4 = to_json($newReading_pair4, { utf8 => 1 });
    my $result_reading_pair4 = $test->request( POST '/reading_pair.json', Content => $newJsonReading_pair4 );
    $result_reading_pair4 = from_json( $result_reading_pair4->content );
    my $idpair4 = $result_reading_pair4->{id};

my $newJsonReading_pair5 = to_json($newReading_pair5, { utf8 => 1 });
    my $result_reading_pair5 = $test->request( POST '/reading_pair.json', Content => $newJsonReading_pair5 );
    $result_reading_pair5 = from_json( $result_reading_pair5->content );
    my $idpair5 = $result_reading_pair5->{id};


my $newJsonReading_pair6 = to_json($newReading_pair6, { utf8 => 1 });
    $result = $test->request( POST '/reading_pair.json', Content => $newJsonReading_pair6 );


subtest "Insertion rules" => sub {
    plan tests => 6;

    my $stored_reading_pair_4 = database->quick_select( 'reading_pair', { id => $idpair4 });
    my $stored_reading_pair_5 = database->quick_select( 'reading_pair', { id => $idpair5 });
    is( $stored_reading_pair_4->{documentid1}, '9782744106170', 'conversion ean->isbn well performed (1/2)');
    is( $stored_reading_pair_4->{idtype1}, 'isbn', 'conversion well performed (2/2)');
    is( $stored_reading_pair_5->{documentid1}, 'id11', 'alphabetical order respected (1/4)');
    is( $stored_reading_pair_5->{documentid2}, 'id22', 'alphabetical order respected (2/4)');
    is( $stored_reading_pair_5->{idtype1}, 'idtype11', 'alphabetical order respected (3/4)');
    is( $stored_reading_pair_5->{idtype2}, 'idtype22', 'alphabetical order respected (4/4)');


};

subtest "Get by id Route" => sub {
    plan tests => 2;
    my $id = database->quick_select("reading_pair", $newReading_pair)->{id};

    my $resJson = $test->request( GET "/reading_pair/$id.json" );
    my $res = from_json( $resJson->content );
    my $resdata = $res->{data};


        # test about expected values of the fields
        # delete all keys which are irrelevant for test
    foreach my $key ( keys %{$resdata} ){
        delete $resdata->{$key} unless grep {$key eq $_} @attributelist;
    }

    $resdata->{securitytoken} = $newReading_pair->{securitytoken};
    is_deeply( $resdata, $newReading_pair,  "get the right resource with id");



        #test about the error returned by function
    is( $res->{'error'}, undef , "no error returned");
 
};

subtest "Search engine" => sub {
    plan tests => 3;
 
    my $resJson = $test->request( GET "/reading_pair.json?documentid1=id11&documentid2=id31");
    my $res = from_json( $resJson->content );
    my $resdata = $res->{data};


    push @attributelist, "nb";
    foreach my $key ( keys %{$resdata->[0]} ){
        delete $resdata->[0]->{$key} unless grep {$key eq $_} @attributelist;
    }


    is_deeply($resdata->[0], $newReading_pair6, "research successful");
    is(scalar keys @$resdata, 1, "only the expected search");
        #test about the error returned by function
    is( $res->{'error'}, undef , "no error returned");
};

subtest "increment field" => sub {
    plan tests => 2;
    
    my $id = database->quick_select("reading_pair", $newReading_pair)->{id};
    my $resJson = $test->request( POST "/reading_pair/$id.json/increment/nb?securitytoken=".$newReading_pair->{securitytoken} );


    my $incrementedReading_pairInDB = database->quick_select("reading_pair", { id => $id } );
    is( $incrementedReading_pairInDB->{nb}, 2, "correctly incremented" ); 

    $resJson = $test->request( POST "/reading_pair/$id.json/increment/nb?step=2&securitytoken=".$newReading_pair->{securitytoken});
    $incrementedReading_pairInDB = database->quick_select("reading_pair", { id => $id } );
    is( $incrementedReading_pairInDB->{nb}, 4, "correctly incremented" );

};


subtest "pertinent suggestions" => sub {
    plan tests => 2;

     
    my $resJson = $test->request( GET "/getsuggestion/id11/idtype11");
    my $res = from_json( $resJson->content )->{data};
    is( scalar @{ $res }, 3, "good number of suggestions");

    subtest "good order of suggestions" => sub{
        plan tests => 3;
        is ( $res->[0]->{documentid}, "id31", "first place correct");
        is ( $res->[1]->{documentid}, "id12", "second place correct");
        is ( $res->[2]->{documentid}, "id22", "third place correct");
    }; 
};

eval{ database->rollback; };
database->{AutoCommit} = 1;
1;
