SET FOREIGN_KEY_CHECKS=0;

--
-- Table structure for table subscription
--

DROP TABLE IF EXISTS user;
CREATE TABLE user(
    id int(4) NOT NULL auto_increment,
    email VARCHAR(255),
    login VARCHAR(255) NOT NULL,
    password VARCHAR(255) NOT NULL,
    PRIMARY KEY(id)
);

DROP TABLE IF EXISTS resource_comment;
CREATE TABLE resource_comment(
    id int(11) NOT NULL auto_increment,
    message VARCHAR(35),
    resource_id int(11),
    resource_type VARCHAR(40),
    nb int(5),
    creationdate date,
    securitytoken varchar(32),
    exportemail varchar(255),
    PRIMARY KEY(id),
    FOREIGN KEY (securitytoken) REFERENCES librarian(id)
);

DROP TABLE IF EXISTS librarian;
CREATE TABLE librarian(
    id VARCHAR(32),
    email VARCHAR(100) NOT NULL,
    name VARCHAR(50) NULL DEFAULT NULL,
    creationdate date NOT NULL,
    activationdate date DEFAULT NULL,
    lastshare date DEFAULT NULL,
    nbaccess int(11) DEFAULT 0,
    PRIMARY KEY(id)
);

DROP TABLE IF EXISTS reading_pair;
CREATE TABLE reading_pair(
    id integer PRIMARY KEY AUTO_INCREMENT,
    documentid1 varchar(14) NOT NULL,
    documentid2 varchar(14) NOT NULL,
    idtype1 varchar(10) NOT NULL,
    idtype2 varchar(10) NOT NULL,
    last_insert date NOT NULL,
    nb int(11) DEFAULT '0',
    timestamp timestamp DEFAULT CURRENT_TIMESTAMP,
    nb_this_month int(11) DEFAULT '1',
    exportemail varchar(100),
    securitytoken varchar(32),
    KEY (idtype1),
    KEY (idtype2),
    KEY (documentid1),
    KEY (documentid2),
    FOREIGN KEY securitytoken_reading(securitytoken) REFERENCES librarian(id)
);

DROP TABLE IF EXISTS subscription;
CREATE TABLE subscription ( -- information related to the subscription
    id int(11) NOT NULL auto_increment, -- unique key for this subscription
    -- subscription info
    title mediumtext, -- in biblio table in KOHA : title (without the subtitle) from the MARC record (245$a in MARC21)
    issn mediumtext, -- in biblioitem table in KOHA : ISSN (MARC21 022$a)
    ean varchar(13) default NULL,-- in biblioitem table in KOHA
    publishercode varchar(255) default NULL, -- in biblioitem table in KOHA : publisher (MARC21 260$b)
    -- subscription_frequency info --
    sfdescription TEXT NOT NULL,
    unit ENUM('day','week','month','year') DEFAULT NULL,
    unitsperissue int(11)  NOT NULL DEFAULT '1',
    issuesperunit int(11) NOT NULL DEFAULT '1',
    -- subscription_numberpatterns info -- 
    label VARCHAR(255),
    sndescription TEXT NOT NULL,
    numberingmethod VARCHAR(255),
    label1 VARCHAR(255) DEFAULT NULL,
    add1 int(11) DEFAULT NULL,
    every1 int(11) DEFAULT NULL,
    whenmorethan1 int(11) DEFAULT NULL,
    setto1 int(11) DEFAULT NULL,
    numbering1 VARCHAR(255) DEFAULT NULL,
    label2 VARCHAR(255) DEFAULT NULL,
    add2 int(11) DEFAULT NULL,
    every2 int(11) DEFAULT NULL,
    whenmorethan2 int(11) DEFAULT NULL,
    setto2 int(11) DEFAULT NULL,
    numbering2 VARCHAR(255) DEFAULT NULL,
    label3 VARCHAR(255) DEFAULT NULL,
    add3 int(11) DEFAULT NULL,
    every3 int(11) DEFAULT NULL,
    whenmorethan3 int(11) DEFAULT NULL,
    setto3 int(11) DEFAULT NULL,
    numbering3 VARCHAR(255) DEFAULT NULL,
    -- mana data --
    nbofusers int(11) DEFAULT NULL, -- number of users using this model
    lastimport DATE NOT NULL, -- date of the last import
    creationdate DATE NOT NULL, -- date of the creation
    exportemail VARCHAR(255) DEFAULT NULL,
    kohaversion VARCHAR(255) DEFAULT NULL,
    language varchar(255) default NULL, -- language of the description and label of numbering pattern and frequency
    securitytoken varchar(32) DEFAULT NULL,
    PRIMARY KEY (id),
    KEY (issn(10)),
    KEY (title(10)),
    KEY (publishercode(13)),
    KEY (ean),
    FOREIGN KEY (securitytoken)  REFERENCES librarian(id)
);  -- ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

DROP TABLE IF EXISTS report;
CREATE TABLE report ( -- information related to the report
    id int(11) NOT NULL auto_increment,
    -- report infos
    savedsql longtext NOT NULL,
    report_name varchar(255) NOT NULL,
    notes VARCHAR(255),
    type VARCHAR(255),
    -- mana infos
    nbofusers INTEGER DEFAULT NULL, -- number of users using this model
    lastimport DATE NOT NULL, -- date of the last import
    creationdate DATE NOT NULL, -- date of the creation
    exportemail VARCHAR(255) DEFAULT NULL,
    kohaversion VARCHAR(255) DEFAULT NULL,
    language VARCHAR(255) default NULL, -- language of the description and label of numbering pattern and frequency
    securitytoken VARCHAR(32) DEFAULT NULL,
    report_group TEXT NULL default NULL,
    PRIMARY KEY(id),
    FOREIGN KEY securitytoken_report(securitytoken) REFERENCES librarian(id) ON DELETE SET NULL
);

--
-- Table structure for table comment
--

DROP TABLE IF EXISTS resource_comment;
CREATE TABLE resource_comment (
    id int(11) auto_increment,
    message VARCHAR(35),
    resource_id int(11),
    resource_type VARCHAR(40),
    nb int(5),
    creationdate DATE,
    securitytoken VARCHAR(32),
    exportemail varchar(255),
    PRIMARY KEY (id),
    FOREIGN KEY securitytoken_comment(securitytoken) REFERENCES librarian(id) ON DELETE SET NULL
);

DROP TABLE IF EXISTS review;
CREATE TABLE review (
    id int(11) auto_increment,
    review TEXT NOt NULL,
    reviewid int(11) NOT NULL,
    documentid VARCHAR(20) NOT NULL,
    idtype VARCHAR(10) NOT NULL,
    creationdate DATE NOT NULL,
    exportemail VARCHAR(255),
    kohaversion VARCHAR(255),
    language VARCHAR(255) NOT NULL,
    securitytoken VARCHAR(32),
    PRIMARY KEY (id),
    KEY (review(10)),
    KEY (documentid(15)),
    KEY (idtype),
    FOREIGN KEY securitytoken_review(securitytoken) REFERENCES librarian(id) ON DELETE SET NULL
);

SET FOREIGN_KEY_CHECKS=1;
